<?php
echo "<pre>";
echo "<h2>addslashes() function</h2>";
$str="insert into abc values(12,'ashish','m','p001');";
echo "original string : ".$str."<br><br>";
$str1=addslashes($str);					//puts slashes where special characters are present in a string as ',
print_r("Slashed String is : ".$str1."<br>");




echo "<h2>chop()  function</h2>";
$str2=chop("   ashish...","\t.");			//chop strings from right and eliminates (.)
echo "choped 1 string : ".$str2."<br>";
$str3=chop(" harshITA","A..Z");				//chop capital letters from right.
echo "choped 2 string : ".$str3."<br>";
$str4=chop("\t\t SARthak","a..z");			//chop small alphabets from right side of the string.
echo "choped 3 string : ".$str4."<br>";




echo "<h2>chunk_split()  function</h2>";
echo "Demo1 string is : here's the new i20 vts engine series model<br><br>";
$str5=chunk_split("here's the new i20 vts engine series model",3,".\t");//chunks the string of size 3 and puts (.\t) and end of each chunk.
echo "Chunked string is : ".$str5."<br>";




echo "<h2>count_chars() function</h2>";
$str6="here's the new i20 vts engine series model";
$str7=count_chars($str6,3);				//prints string of unique characters in the given string.
echo "Unique characters in above string are ::  ".$str7;





echo "<h2>crc32()  function</h2>";

$int1=crc32($str6);			//converts a string into unsigned int by performing checksum.
echo "Decimal format of Demo1 string is ".$int1."<br>";
printf("Hexadecimal format of Demo1 is  %x\n",$int1);





echo "<h2>crypt() function</h2>";

$str8="welcome to ttnd";
echo "String is  ".$str8."<br>";
$str9=crypt($str8,"passwd");		//creates a crypted text from string using a salt string additionally.
echo "Cryted String is  ".$str9."<br>";




echo "<h2>htmlentities  function</h2>";
$str10="<u>hello !! how's you..<br></u>";
echo "original string is ".$str10."<br>";
$str11=htmlentities($str10,ENT_COMPAT,"UTF-8");		//Will convert double-quotes and leave single-quotes alone.
echo "Converting the double quotes :  ".$str11."<br>";
$str12=htmlentities($str10,ENT_QUOTES,"UTF-8");		//Will convert both double and single quotes.
echo "Converting all quotes  :   ".$str12."<br>";






echo "<h2>htmlspecialchars  function</h2>";

$str13="\x8Fc!!!";
echo "String is  ::   \x8Fc!!!<br>";
$str14=htmlspecialchars($str13,ENT_IGNORE);   //Silently discard invalid code unit sequences instead of returning an empty string.
echo "html entity from string  ::   ".$str14;




echo "<h2>Join()  Function</h2>";
$arr=array('Ashish','Pandey','Sarthak','harshita','saini');		//Input array of several elements.
echo "Input Array is : <br>";
print_r($arr);

$str15= join("/",$arr);				//String from an array elements.
echo "<br>Output String is : ";
print_r($str15);




echo "<h2>md5   function</h2>";
$str16="SecurePaaswd";
$str17=md5($str16);				//Generates a hash code by the md5 algorithm.
echo "md5 hasshing of ".$str16." is given as  ".$str17;





echo "<h2>parse_str()  function</h2>";
$str18="http://localhost/php_day3/login.php?uname=ashish+pandey&&upass=ttnd";
print_r($str18);
parse_str($str18,$arr2);		//this array contains the parsed strings 
echo "<br><br>The parsed info is as follows :- <br>";
print_r($arr2);




echo "<h2>str_replace()  function.</h2>";
$str19="winn";
$str20="loss";
$str21="winner always winnes the race";		//replaces the search string with the needle string

echo "original string is ".$str21."<br>";
echo "Replaced string is ";
echo str_replace($str19,$str20,$str21);




echo "<h2>strstr()  function</h2>";
$str22="The earth is 70% covered with water bodies"; 	//Prints the string characters before the character being searched in the string 
echo "string is ::   ".$str22."<br>";
$str23="70%";
$str24=strstr($str22,$str23,true);
echo "The string before ".$str23." is   ::   ".$str24;





echo "<h2>stncmp()  function</h2>";
$str25="I am the new King";
$str26="I am the new boss";
$int3=strncmp($str25,$str26,13);		//compares two string for the specified length.
if($int3 == 0) echo "$str25 equal to $str26 till 13th index";
else if($int3 < 0) echo "value of $str25 is less than $str26";
else echo "value of $str25 is greater than $str26";





echo "<h2>substr()  function</h2>";
echo "string is  ::  ".$str25."<br><br>";
echo "substr of the string 3,4 is  ".substr($str25,3,4)."<br>";//sub string starts from 3rd position till 4 characters.
echo "substr of the string -8,-5 is ".substr($str25,-8,-5)."<br>";  //If start negative, returned string will start at the start'th character from the end of string.

?>
