1. use test;

2. 
  (i)  create table students(id smallint unsigned auto_increment primary key,name varchar(100));
  (ii)  create table enrollments(studentid smallint unsigned,name varchar(100) default null);

  show create table students;
  show create table enrollments;

3. alter table enrollments engine=InnoDb;

4. alter table enrollments add foreign key (studentid) references students(id);

5. alter table enrollments change name name varchar(50) default 'New Student' not null;
   
   desc enrollments;

6. rename table enrollments to t2;

7. drop table if exists enrollments;

8. drop table students,t2;
   show tables;
   
9. use world;
   create table europe as select Name,Population from Country where Continent='Europe';
    create table asia as select Name,Population from Country where Continent='Asia';

   (i) select * from europe;
       select * from asia;

   (ii) select Name,Population from europe where Population < 500000 union select Population,Name from asia where Population > 1000000;

10. create database new_world;

11. create table db_demo like db_test;

