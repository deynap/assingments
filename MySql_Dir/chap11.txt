1. create database world_copy;

2. use world_copy;
   create table City like world.City;
   insert into City select * from world.City;
   create table Country like world.Country;
   insert into Country select * from world.Country;
   create table CountryLanguage like world.CountryLanguage;
   insert into CountryLanguage select * from world.CountryLanguage;

3.  use world_copy;
    insert into City(Name,CountryCode,Population,District) values('Sarah city','USA',1,'California');

4. select LAST_INSERT_ID();

5. replace into City(Name,CountryCode,Population,District) values('Steve city','USA',1,'California');
   Yes, it replaces the original 'Sarah City' from the table.

6. set sql_mode='';

7. create table t3(id tinyint);

8. insert into City(Name,Population) values('test',500);
   select * from City where Name='test' and Population=500;

9. set sql_mode='traditional';

10. insert into City(Name,Population) values('test',500);

11. replace into Country(Name,Continent) values('gre','Europe');

12. update Country set GNP=123000 where Name='india';

13. update Country set Population=Population+Population*0.1 where Name='india';

14. create table European as select * from Country where Continent='Europe';

15. delete from Country where Name='india';

