<?php
echo "<pre>";

echo "Use array_count_values() on an array.<br>";

$arr = array('Peter','Piper','picked','a','peck','of','pickled','peppers','A','peck','of','pickled','peppers','Peter','Piper','picked');
echo "Arrray is <br>";
print_r($arr);

echo "<br>Count of values is : ";
print_r(array_count_values($arr));				//function to count number of occurance of these value of the input array.

echo "******************************************************************";

 echo "<br>Script to combine two arrays.<br>";

 $data=array('A','B','C');						//First Array assigned by $data
 echo "First array <br>";
 print_r($data);
 $details=array('ashish',					//Second array assigned by variable details.
   array('sankalp','deepak','manali','richa','anurag'),
   array('harshita','sarthak','manish')
 );
 echo "<br>Second array <br>";
 print_r($details);

 $master = array_combine($data,$details);		//New Array after combining two arrays and is assigned to variable master.
 echo "<br>Final Array <br>";
 print_r($master);			//prints content of new array.

echo "******************************************************************";

 echo "<br>Script to chunk an array using array_chunk() function.<br>";
 $array=array('Peter','Piper','picked','a','peck','of','pickled','peppers','A','peck','of','pickled','peppers','Peter','Piper','picked');
 echo "Array is : <br>";
 print_r($array);

 $ans=array_chunk($array,2.3);
 echo "Array Chunks are : <br>";
 print_r($ans);

echo "******************************************************************";

 echo "<br>Script to perform merge of two arrays.<br>";

 $details=array('ashish',					//Taken as the first array to be merged.
   array('sankalp','deepak','richa','manali','anurag'),
   array('harshita','sarthak','manish')
 );
 echo "First array : <br>";
 print_r($details);
 
 $data=array('A','B','C','D','E');				//taken as second array.
 echo "Second Array : <br>";
 print_r($data);

 $master = array_merge($details,$data);				//merge two arrays and is assigned to master variable.
 echo "Final Merged Array : <br>";
 print_r($master);					//Prints the new array

echo "******************************************************************";
echo "<br>Script to show difference betweeen two arrays<br>";

$array1 = array("a" => "green", "red", "blue", "red");
echo "First array is : <br>";
print_r($array1);
$array2 = array("b" => "green", "yellow", "red","blue","ashish");
echo "Second array is : <br>";
print_r($array2);
$result = array_diff($array1, $array2);		//It takes difference of the values of two arrays.
echo "<br>Final difference array is : ";
print_r($result);		//Prints the value that exists in first array but not in second array.

echo "******************************************************************";

echo "<br>Script that shows array intersection<br>";

$array1 = array('ashish','harshita','sarthak','manish','devanshu','ankita');
echo "<h2>First array :: </h2>";
print_r($array1);
$array2 = array('sankalp','deepak','richa','manali','cab'=>'anurag','ashish');
echo "<h2>Second array :: </h2>";
print_r($array2);
$result = array_intersect($array1, $array2);
echo "<h2>Array after INTERSECTION ::</h2>";
print_r($result);			//prints the intersected value of two arrays







function cabmap($arr1, $arr2)
{
    return("The person $arr1 and $arr2 are coming in Cab");
}

function ttndmap($arr1, $arr2)
{
    return(array($arr1 => $arr2));
}

$c = array_map("ttndmap", $array1,$array2);
echo "<H2><br>mapping Two arrays as :: <br></H2>";
print_r($c);

$d = array_map("cabmap", $array1,$array2);		//Maps two arrays only if no. of elements are same in both the arrays.
echo "<H2><br>Mapping persons in the cab<br></H2>";
print_r($d);






$array3=array('cab'=>'ashish','harshita','sarthak');
$result = array_merge_recursive($array2, $array3);		//Recursively adds values with same keys into an array.
echo "<H2>Merge Recursive array result :: <br></H2>";
print_r($result);





echo "<h2>Array Push function </h2>";
$arrand=array("mango","banana","lichi","pineapple");
echo "original array <br>";
print_r($arrand);
array_push($arrand, "apple", "raspberry");	//Adds new value(s) to the current array.
echo "Result array is ::";
print_r($arrand);				//Prints the new array.






echo "<h2>Array Reduce function </h2>";
$init=array(10,20,30,40,50,60);
function evaluate($returnval,$incrementval)
{
 $returnval += $incrementval;
 return $returnval;
}

echo "reduced array is :";
print_r(array_reduce($init,"evaluate"));		//Reduces an array into a single value through a function.





echo "<h2>Array search function</h2>";
$val=30;					//If needle is a string, the comparison is done in a case-sensitive manner.
$res=array_search($val,$init);		//Returns the index of the value in the array.
echo "Searched value ".$val." is at ".($res+1)." index in the given array.";




echo "<h2>Array Shift Function</h2>";
$ans=array_shift($init);		//Returns the shifted value, or NULL if array is empty or is not an array.
echo "The shifted value out off the array is : ".$ans;





echo "<h2>Array Unshift Function</h2>";
$l=array_unshift($init,7,8,9);			//Returns the new number of elements in the array.
echo "No. of elements in the array now are : ".$l;
print_r($init);





echo "<h2>Extract Function on Arrays</h2>";
$m=10;
$n=20;
echo "value of m is ".$m."<br>";
echo "value of n is ".$n."<br>";
$az=array('m'=>40,'n'=>30,'o'=>60,'p'=>70);
echo "<br><br>After extracting<br><br>";
extract($az,EXTR_OVERWRITE,"ash");				//If flags is not specified, it is assumed to be EXTR_OVERWRITE.

echo "value of m is ".$m."<br>";
echo "value of n is ".$n."<br>";
echo "value of o is ".$o."<br>";
echo "value of p is ".$p."<br>";





echo "<br><h2>Sorting Arrays Function</h2><br>";
sort($az);						//Sorts array in alpha-numerical order.
print_r($az);






echo "<h2>usort() Function </h2>";
$dir  = '/var/www/html/php_day5/'; 
$files = scandir($dir); 
$array = array();
foreach($files as $file)
{
    if($file != '.' && $file != '..')
    {
	$last_modified = filemtime($dir.$file);
	$array[] = array('file'         => $file,
                     'timestamp'    => $last_modified,
                     'date'         => date ("F d Y H:i:s", $last_modified)
		    );
    }
} 
usort($array, create_function('$a, $b', 'return strcmp($a["timestamp"], $b["timestamp"]);'));
echo '<h3>Dir :  '.$dir.'</h3>'; 
print_r($array);






echo "<h2>Shuffle function in arrays</h2>";
$ax=array(90,80,70,60,50,40,30,20,10);
$bol=shuffle($ax);				//Shuffles elements of the aray in a random order.
if($bol == true)
echo "The Shuffle was successful <br>";
else echo "Shuffle was unsuccessful<br>";
print_r($ax);
?>
