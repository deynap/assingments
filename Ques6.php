<?php
 echo "          ****php script to implement anonymous functions****<br>";
 
 $wildcat="sher";
 $animal=function() use ($wildcat){				//anonymous function.
 
 echo "<br>The animal is a ".$wildcat." !!! <br>";			//prints animal is a sher
 };
  
 $wildcat="billi";
 $animal();

 // Inherit $wildcat value is taken when function is defined not when it is called. 

 $wildcat="cheetah";
 $animal=function() use (&$wildcat) {				//Anonymous function with reference.
 
 echo "<br>The animal is a ".$wildcat." !!!<br>";			//prints animal is a tiger
 };
 
 $wildcat="tiger";
 $animal();

 // Inherit $wildcat is passed by reference. Therefore, updated value is passed to the function.
 
 echo "<br>          ****php script to implement variable functions****<br>"; 
 
 function plant(){									//Function plant.
  echo "<br> plants are the major source of life on earth. <br>";
 }

 function ecosys($args){								//Function ecosys with one argument.
  echo "<br> plants are the major contributor of ".$args." in this ecosystem. <br>";
 }

 function oxygen($string,$float){							//Function oxygen with two arguments.
  echo "<br> $string is about $float% in gaseous form on our earth. <br>";
 }

 $var = 'plant';				//variable var is assigned for plant.
 $var();

 $var = 'ecosys';				//variable var is assigned for ecosys.
 $var('oxygen');

 $var = 'oxygen';				//variable var is assigned for oxygen.
 $var('oxygen',70.24);
		//The variable functions are called by a variable which is assigned with the name of the function.
?>
