<?php
 echo "          ****php script to implement user defined functions with default arguments****<br>";

  function calcTotalMrks($mrks1, $mrks2=75) 		//Function calculate Total Marks with default argument $marks.
   {
   $sum = $mrks1 + $mrks2; 				//Sum of two marks.
   echo "<br>Total marks : $sum out of 200<br>"; 
   }
    
    $maths = 86; 
    $science = 55; 
    calcTotalMrks($maths, $science); 			//Calling Function calcTotalMrks with two arguments.
    calcTotalMrks($maths);				//Calling Function calcTotalMrks with one argument.
    calcTotalMrks($science);				//The second argument will be considered with default value.
?>
