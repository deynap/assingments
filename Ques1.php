<?php
 echo "          ****php script that implements variables scopes****<br>";

	$g_var=10;
	function abc(){
		global $g_var;				//global variable
		echo 'global access g_var ='.$g_var."<br>";
		$l_var=0;				//local variable
		static $s_var=5;			//static variable
		echo 'value of l_var = '.$l_var."<br>";
		echo 'value of s_var = '.$s_var."<br>";
		$l_var++;				//value of local variable doesn't increment.
		$s_var++;				//value of static variable increments.
	}
	echo 'value of g_var = '.$g_var."<br>";		//access global variable outside a function.
	abc();
	abc();						//calling of function.
	abc();
?>
