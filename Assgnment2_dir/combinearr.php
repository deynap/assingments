<?php					//Script to combine two arrays.
 echo "<pre>";

 $data=array('A','B','C');						//First Array assigned by $data

 $details=array('ashish',					//Second array assigned by variable details.
   array('sankalp','deepak','manali','richa','anurag'),
   array('harshita','sarthak','manish')
 );

 $master = array_combine($data,$details);		//New Array after combining two arrays and is assigned to variable master.

 print_r($master);			//prints content of new array.

?>
