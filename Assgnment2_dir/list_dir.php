<?php					//Lists all files in a directory.
echo "<pre>";
$dir = "/var/www/html";				//directory to be listed down
$dh  = opendir($dir);				//opens the directory and access the files present in it.
while (false !== ($filename = readdir($dh))) {		//reads the files till all files are being read out.
    $files[] = $filename;		//stores files in an array.
}

sort($files);				//sort files in ascending order alphabetically.
echo "List of files in ".$dir." directory in ASCENDING Order.<br>";
print_r($files);

rsort($files);				//sort files in descending order in alphabetical order.
echo "List of files in ".$dir." directory in DESCENDING Order.<br>";
print_r($files);

?>

