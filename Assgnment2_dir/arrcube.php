<?php				//create an array(integer) which has all values as cubes of original array
 echo "<pre>";
 function cubearray(array &$ar){		//function with array as parameter.
 $as=array();
 foreach($ar as $k=>$v)				//foreach array element.
 {
  $as[$v]=$v*$v*$v;				//cube of original array's value assigned to new array's value.
  
 }
 
 return $as;					//returning new array formed.
 }
 $arr=array(1,2,3,4,5,6,7,8,9);
 echo "Input Array is : <br>";
 print_r($arr);					//prints original array.
 $as=cubearray($arr);

 echo "<br>Output Cubed Array is : <br>";
 print_r($as);					//prints cubed value array.
?>
