<?php					//Script to perform merge of two arrays.
 echo "<pre>";

 $details=array('ashish',					//Taken as the first array to be merged.
   array('sankalp','deepak','richa','manali','anurag'),
   array('harshita','sarthak','manish')
 );

 $data=array('A','B','C','D','E');				//taken as second array.

 $master = array_merge($data,$details);				//merge two arrays and is assigned to master variable.

 print_r($master);					//Prints the new array
?>
