<?php
echo "<pre>";
$dir  = '/var/www/html/php_day5/'; 
$files = scandir($dir); 
$array = array();
foreach($files as $file)
{
    if($file != '.' && $file != '..')
    {
	$last_modified = filemtime($dir.$file);
	$array[] = array('file'         => $file,
                     'timestamp'    => $last_modified,
                     'date'         => date ("F d Y H:i:s", $last_modified)
		    );
    }
} 
usort($array, create_function('$a, $b', 'return strcmp($a["timestamp"], $b["timestamp"]);'));
echo '<h3>Dir :  '.$dir.'</h3>'; 
print_r($array);
?>
