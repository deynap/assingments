<?php
 echo "          ****php script to call the callback given by the first parameter****<br>";

 $a=10;
 $b=13;
 function eval_sum($a,$b){			//user defined callback Function eval_sum().
    echo "The sum is = ".($a+$b)."<br>";		//prints the sum of two arguments.
  }
 
 eval_sum($a,$b);				//Calling of function with two arguments.
 
 call_user_func('eval_sum',10,20);		//Use of call_user_func() which accepts user defined functions as a parameter.
 						//function name and its arguments are passed as parameters.
 $a= 5.2;
 $b= 6.7;
 
 call_user_func('eval_sum',$a,$b);		//call_user_func takes variables as arguments.
 
 
 $c= array(20,43);				//an array with assigned to variable c.
 
 usort($c,'eval_sum');				//usort() Function takes first parameter an array and second parameter as the Function name.

 			//usort() and call_user_func() both are used to call user defined callback functions.
?>
